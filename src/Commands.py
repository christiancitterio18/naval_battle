class Commands:
    """
    Implements server methods
    """

    USER_COMMANDS: dict = {
        "Rules": "Shows the rules of the game",
        "Play": "Starts a match AI vs Player",
        "Quit": "Exits the program",
    }

    GAME_RULES: dict = {
        "1": "Each player has: 4 ships from one square; 3 from two squares; "
        + "2 from three squares and 1 from four squares.",
        "2": "Each player places their ships on a 10x10 table "
        + "(lines A through J and columns 1 through 10)"
        + ", ships cannot be placed diagonally.",
        "3": 'Each player in turn "shoots" a shot (say the name of the square, e.g. A1), '
        + "the shot can be empty or hit a ship.",
        "4": "If a player hits a ship, the opponent says that the ship "
        + "was hit or that it was hit and sunk.",
        "5": "The turn ends, and the opponent takes his turn.",
        "6": "The player who has no more ships loses.",
    }

    # -------------------------------------------------------------------------------------------
    #  Server methods

    def help_command(self) -> dict:
        """
        Returns a description of the available commands
        :return: dict | command list
        """
        return self.USER_COMMANDS

    def rules_command(self) -> dict:
        """
        Returns the game rules
        :return: dict | game rules
        """
        return self.GAME_RULES
