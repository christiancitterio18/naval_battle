import json
import socket

from src import DataPreparation, GameSetup, GameTurn


class Server(object):

    PORT = 7070
    SERVER = '127.0.0.1'
    ADDRESS = (SERVER, PORT)

    HEADER = 64
    FORMAT = 'utf-8'
    BUFFER = 1024

    server_ships: dict = {}
    enemy_shoots: list = []
    my_shoots: list = []
    server_boards: dict = {}
    is_my_turn: bool = False

    def __init__(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            print('[SERVER] Starting...')

            s.bind(self.ADDRESS)
            s.listen()

            clientsocket, address = s.accept()

            with clientsocket:

                print(f'[SERVER] Connected by {address}')

                print('[SERVER] Setting up for game...')
                clientsocket.send('[SERVER] Setting up for game...'.encode(self.FORMAT))

                self.server_ships = GameSetup.random_ships_creation()
                (
                    self.server_boards['ships_board'],
                    self.server_boards['shoot_board'],
                ) = GameSetup.create_boards(self.server_ships)

                print('Ready')

                while True:

                    if self.is_my_turn:
                        print('Preparing Shoot!')
                        # my turn
                        shoot_data: dict = DataPreparation.prepare_command_data('shoot',
                                                                                coordinates=GameTurn.
                                                                                ai_turn(set(self.my_shoots)))
                        self.my_shoots.append(shoot_data['data']['coordinates'])
                        clientsocket.send(json.dumps(shoot_data, indent=2).encode(self.FORMAT))

                        print('Shoot to coordinates: ', self.my_shoots[-1])

                        res_from_client: dict = json.loads(clientsocket.recv(self.BUFFER).decode(self.FORMAT))

                        print(res_from_client['result'] + '!')

                        if res_from_client['game_over']:
                            print('You win!')
                            GameTurn.update_shoot_board(self.server_boards['shoot_board'],
                                                        res_from_client['result'],
                                                        self.my_shoots[-1])
                            self.print_boards()
                            break
                        else:

                            print('Updating our boards...')
                            GameTurn.update_shoot_board(self.server_boards['shoot_board'],
                                                        res_from_client['result'],
                                                        self.my_shoots[-1])
                            self.is_my_turn = False

                            res: dict = {
                                'turn_over': True
                            }

                            clientsocket.send(json.dumps(res, indent=2).encode(self.FORMAT))

                            self.is_my_turn = False
                            print('Our turn ended...')
                    else:
                        # enemy turn
                        print('Waiting for enemy to shoot...')
                        message_from_client: dict = json.loads(clientsocket.recv(self.BUFFER).decode(self.FORMAT))

                        self.enemy_shoots.append(message_from_client['data']['coordinates'])
                        res, self.server_boards['ships_board'] = GameTurn.shoot_command(
                                                                        message_from_client['data']['coordinates'],
                                                                        self.server_boards['ships_board'],
                                                                        self.enemy_shoots,
                                                                        self.server_ships)

                        print('Enemy shot at coordinates: ',  self.enemy_shoots[-1])

                        clientsocket.send(json.dumps(res, indent=2).encode(self.FORMAT))

                        print('Enemy is updating boards...')

                        if res['game_over']:
                            print('We lose!')
                            self.print_boards()
                            break
                        else:
                            res_from_client: dict = json.loads(clientsocket.recv(self.BUFFER).decode(self.FORMAT))

                            if res_from_client['turn_over']:
                                self.is_my_turn = True
                                print('Enemy turn is over...')
                            else:
                                print('Waiting for answer from enemy...')

    def print_boards(self):

        print('\n-----------ships_board----------\n')
        for k, v in self.server_boards['ships_board'].items():
            print(k + ' ' + ''.join(v))

        print('\n-----------shoots_board----------\n')
        for k, v in self.server_boards['shoot_board'].items():
            print(k + ' ' + ''.join(v))

        print()
