
def prepare_command_data(command="", **values) -> dict:
    """
    Returns the command in the format the server accepts
    :param command: str | the command to execute
    :param values: dict | additional data for the command
    :return: dict | the data to execute on server side
    """
    command_data: dict = {"command": command, "data": values}

    return command_data
