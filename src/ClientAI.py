import json
import socket

from src import DataPreparation, GameSetup, GameTurn


class ClientAi(object):

    PORT = 7070
    SERVER = '127.0.0.1'
    ADDR = (SERVER, PORT)

    HEADER = 64
    FORMAT = 'utf-8'
    BUFFER = 1024

    ai_ships: dict = {}
    enemy_shoots: list = []
    my_shoots: list = []
    ai_boards: dict = {}
    is_my_turn: bool = True

    def __init__(self):

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as c:
            c.connect(self.ADDR)
            print('[AI] Setting up for game...')
            self.ai_ships = GameSetup.random_ships_creation()
            self.ai_boards['ships_board'], self.ai_boards['shoot_board'] = GameSetup.create_boards(self.ai_ships)

            print(c.recv(1024).decode(self.FORMAT))

            while True:
                if self.is_my_turn:
                    print('Preparing Shoot!')
                    # my turn
                    shoot_data: dict = DataPreparation.prepare_command_data('shoot',
                                                                            coordinates=GameTurn.
                                                                            ai_turn(set(self.my_shoots)))
                    self.my_shoots.append(shoot_data['data']['coordinates'])
                    c.send(json.dumps(shoot_data, indent=2).encode(self.FORMAT))

                    print('Shoot to coordinates: ', self.my_shoots[-1])

                    res_from_client: dict = json.loads(c.recv(self.BUFFER).decode(self.FORMAT))

                    print(res_from_client['result'] + '!')

                    if res_from_client['game_over']:
                        print('You win!')
                        GameTurn.update_shoot_board(self.ai_boards['shoot_board'],
                                                    res_from_client['result'],
                                                    self.my_shoots[-1])
                        self.print_boards()
                        break
                    else:

                        print('Updating our boards...')
                        GameTurn.update_shoot_board(self.ai_boards['shoot_board'],
                                                    res_from_client['result'],
                                                    self.my_shoots[-1])
                        self.is_my_turn = False

                        res: dict = {
                            'turn_over': True
                        }

                        c.send(json.dumps(res, indent=2).encode(self.FORMAT))

                        self.is_my_turn = False
                        print('Our turn ended...\n')
                else:
                    # enemy turn
                    print('Waiting for enemy to shoot...')
                    message_from_client: dict = json.loads(c.recv(self.BUFFER).decode(self.FORMAT))
                    res, self.ai_boards['ships_board'] = GameTurn.shoot_command(
                            message_from_client['data']['coordinates'],
                            self.ai_boards['ships_board'],
                            self.enemy_shoots,
                            self.ai_ships)
                    self.enemy_shoots.append(message_from_client['data']['coordinates'])

                    print('Enemy shot at coordinates: ', self.enemy_shoots[-1])

                    c.send(json.dumps(res, indent=2).encode(self.FORMAT))

                    print('Enemy is updating boards...')

                    if res['game_over']:
                        print('We lose!')
                        self.print_boards()
                        break
                    else:
                        res_from_client: dict = json.loads(c.recv(self.BUFFER).decode(self.FORMAT))
                        if res_from_client['turn_over']:
                            self.is_my_turn = True
                            print('Enemy turn is over...\n')
                        else:
                            print('Waiting for answer from enemy...')

    def print_boards(self):

        print('\n-----------ships_board----------\n')
        for k, v in self.ai_boards['ships_board'].items():
            print(k + ' ' + ''.join(v))

        print('\n-----------shoots_board----------\n')
        for k, v in self.ai_boards['shoot_board'].items():
            print(k + ' ' + ''.join(v))

        print()
