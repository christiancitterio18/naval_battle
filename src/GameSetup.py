import random

from src import BoardVerification


def create_boards(ships: dict) -> dict:
    """
    Returns the board for each player
    :parametr ships: dict | dictionary containing lists of user ships
    :return: dict | board for each player
    """
    board_template: dict = {
        ' ': ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        'A': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'B': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'C': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'D': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'E': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'F': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'G': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'H': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'I': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'J': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
    }
    board_template2: dict = {
        ' ': ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        'A': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'B': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'C': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'D': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'E': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'F': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'G': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'H': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'I': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
        'J': ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
    }
    board = board_template
    for i in ships:
        for k in ships[i]:
            board = places_ship_on_board(k, board)
    return board, board_template2


def players_ships() -> dict:
    """
    Returns a dict for setting the board of the player
    :return: dict | player ships settings
    """
    ships_coordinates: dict = {'1': [], '2': [], '3': [], '4': []}
    coordinates_already_used: list = []

    for _ in range(0, 4):
        block_1_ship, coordinates_already_used = player_ship_1_creation(coordinates_already_used)
        ships_coordinates['1'].append(block_1_ship)

    for _ in range(0, 3):
        block_2_ship, coordinates_already_used = player_ship_2_creation(coordinates_already_used)
        ships_coordinates['2'].append(block_2_ship)

    for _ in range(0, 2):
        block_3_ship, coordinates_already_used = player_ship_3_creation(coordinates_already_used)
        ships_coordinates['3'].append(block_3_ship)

    block_4_ship, coordinates_already_used = player_ship_4_creation(coordinates_already_used)
    ships_coordinates['3'].append(block_4_ship)

    return ships_coordinates


def places_ship_on_board(available_ship: tuple, board: dict) -> dict:
    """
    Populates the board with ships
    :parametr board: dict | blank board to populate
    :parametr available_ship: tuple | ship's coordinates
    :return: dict | populated board
    """
    if isinstance(available_ship, str):
        temp_available_ship: tuple = tuple(available_ship)
        board[(temp_available_ship[0])][int(temp_available_ship[1])] = 'O'
    else:
        for _ in available_ship:
            temp_available_ship: tuple = tuple(_)
            board[(temp_available_ship[0])][int(temp_available_ship[1])] = 'O'
    return board


def random_ships_creation() -> dict:
    """
    Returns dict,  containing lists of ship for an AI player
    :return: dict | dictionary of lists of tuples
    """
    direction: str = 'HV'
    ships_coordinates: dict = {'1': [], '2': [], '3': [], '4': []}
    coordinates_already_used: list = []

    print('creating ships')

    for _ in range(4):
        ship, coordinates_already_used = rand_ship_1_creation(coordinates_already_used)
        ships_coordinates['1'].append(ship)

    for _ in range(3):
        ship, coordinates_already_used = rand_ship_2_creation(coordinates_already_used, random.choice(direction))
        ships_coordinates['2'].append(ship)

    for _ in range(2):
        ship, coordinates_already_used = rand_ship_3_creation(coordinates_already_used, random.choice(direction))
        ships_coordinates['3'].append(ship)

    ship, coordinates_already_used = rand_ship_4_creation(coordinates_already_used, random.choice(direction))
    ships_coordinates['4'].append(ship)

    print('created ships')
    return ships_coordinates


def rand_ship_1_creation(coordinates_used: list):
    row: str = 'ABCDEFGHIJ'
    col: str = '0123456789'
    coord: list = random.choice(row) + random.choice(col)

    if coord not in coordinates_used:
        coordinates_used.append(coord)
        return coord, coordinates_used
    else:
        return rand_ship_1_creation(coordinates_used)


def rand_ship_2_creation(coordinates_used: list, direction: str) -> tuple[tuple, list]:
    if direction == 'H':
        row: str = 'ABCDEFGHIJ'
        col: str = '12345678'
        coord: list = random.choice(row) + random.choice(col)

        if coord not in coordinates_used:
            coord_p1: str = coord[0] + str(int(coord[1]) + 1)
            coord_m1: str = coord[0] + str(int(coord[1]) - 1)

            if coord_m1 in coordinates_used and coord_p1 in coordinates_used:
                return rand_ship_2_creation(coordinates_used, 'V')
            elif coord_p1 in coordinates_used:
                coordinates_used.append(coord)
                coordinates_used.append(coord_m1)
                return (coord, coord_m1), coordinates_used
            else:
                coordinates_used.append(coord)
                coordinates_used.append(coord_p1)
                return (coord, coord_p1), coordinates_used
        else:
            return rand_ship_2_creation(coordinates_used, random.choice('HV'))

    elif direction == 'V':
        row: str = 'BCDEFGHI'
        col: str = '0123456789'
        coord: list = random.choice(row) + random.choice(col)

        if coord not in coordinates_used:
            coord_p1: str = chr(ord(coord[0]) + 1) + coord[1]
            coord_m1: str = chr(ord(coord[0]) - 1) + coord[1]

            if coord_m1 in coordinates_used and coord_p1 in coordinates_used:
                return rand_ship_2_creation(coordinates_used, 'H')
            elif coord_p1 in coordinates_used:
                coordinates_used.append(coord)
                coordinates_used.append(coord_m1)
                return (coord, coord_m1), coordinates_used
            else:
                coordinates_used.append(coord)
                coordinates_used.append(coord_p1)
                return (coord, coord_p1), coordinates_used
        else:
            return rand_ship_2_creation(coordinates_used, random.choice('HV'))


def rand_ship_3_creation(coordinates_used: list, direction: str) -> tuple[tuple, list]:
    if direction == 'H':
        row: str = 'ABCDEFGHIJ'
        col: str = '234567'
        coord: list = random.choice(row) + random.choice(col)

        if coord not in coordinates_used:
            coord_p1: str = coord[0] + str(int(coord[1]) + 1)
            coord_m1: str = coord[0] + str(int(coord[1]) - 1)

            if coord_m1 in coordinates_used and coord_p1 in coordinates_used:
                return rand_ship_3_creation(coordinates_used, random.choice('HV'))
            elif coord_p1 in coordinates_used:
                coord_m2: str = coord[0] + str(int(coord[1]) - 2)
                if coord_m2 in coordinates_used:
                    return rand_ship_3_creation(coordinates_used, 'V')
                else:
                    coordinates_used.append(coord)
                    coordinates_used.append(coord_m1)
                    coordinates_used.append(coord_m2)
                    return (coord, coord_m1, coord_m2), coordinates_used
            else:
                coord_p2: str = coord[0] + str(int(coord[1]) + 2)
                if coord_p2 in coordinates_used:
                    return rand_ship_3_creation(coordinates_used, 'V')
                else:
                    coordinates_used.append(coord)
                    coordinates_used.append(coord_p1)
                    coordinates_used.append(coord_p2)
                    return (coord, coord_p1, coord_p2), coordinates_used
        else:
            return rand_ship_3_creation(coordinates_used, random.choice('HV'))

    elif direction == 'V':
        row: str = 'CDEFGH'
        col: str = '0123456789'
        coord: list = random.choice(row) + random.choice(col)

        if coord not in coordinates_used:
            coord_p1: str = chr(ord(coord[0]) + 1) + coord[1]
            coord_m1: str = chr(ord(coord[0]) - 1) + coord[1]

            if coord_m1 in coordinates_used and coord_p1 in coordinates_used:
                return rand_ship_3_creation(coordinates_used, random.choice('HV'))
            elif coord_p1 in coordinates_used:
                coord_m2: str = chr(ord(coord[0]) - 2) + coord[1]

                if coord_m2 in coordinates_used:
                    return rand_ship_3_creation(coordinates_used, 'H')
                else:
                    coordinates_used.append(coord)
                    coordinates_used.append(coord_m1)
                    coordinates_used.append(coord_m2)
                    return (coord, coord_m1, coord_m2), coordinates_used
            else:
                coord_p2: str = chr(ord(coord[0]) + 2) + coord[1]
                if coord_p2 in coordinates_used:
                    return rand_ship_3_creation(coordinates_used, 'H')
                else:
                    coordinates_used.append(coord)
                    coordinates_used.append(coord_p1)
                    coordinates_used.append(coord_p2)
                    return (coord, coord_p1, coord_p2), coordinates_used
        else:
            return rand_ship_3_creation(coordinates_used, random.choice('HV'))


def rand_ship_4_creation(coordinates_used: list, direction: str) -> tuple[tuple, list]:
    if direction == 'H':
        row: str = 'ABCDEFGHIJ'
        col: str = '3456'
        coord: list = random.choice(row) + random.choice(col)

        if coord not in coordinates_used:
            coord_p1: str = coord[0] + str(int(coord[1]) + 1)
            coord_m1: str = coord[0] + str(int(coord[1]) - 1)

            if coord_m1 in coordinates_used and coord_p1 in coordinates_used:
                return rand_ship_4_creation(coordinates_used, random.choice('HV'))
            elif coord_p1 in coordinates_used:
                coord_m2: str = coord[0] + str(int(coord[1]) - 2)
                if coord_m2 in coordinates_used:
                    return rand_ship_4_creation(coordinates_used, random.choice('HV'))
                else:
                    coord_m3: str = coord[0] + str(int(coord[1]) - 3)
                    if coord_m3 in coordinates_used:
                        return rand_ship_4_creation(coordinates_used, 'V')
                    else:
                        coordinates_used.append(coord)
                        coordinates_used.append(coord_m1)
                        coordinates_used.append(coord_m2)
                        coordinates_used.append(coord_m3)
                        return (coord, coord_m1, coord_m2, coord_m3), coordinates_used
            else:
                coord_p2: str = coord[0] + str(int(coord[1]) + 2)
                if coord_p2 in coordinates_used:
                    return rand_ship_4_creation(coordinates_used, random.choice('HV'))
                else:
                    coord_p3: str = coord[0] + str(int(coord[1]) + 3)
                    if coord_p3 in coordinates_used:
                        return rand_ship_4_creation(coordinates_used, 'V')
                    else:
                        coordinates_used.append(coord)
                        coordinates_used.append(coord_p1)
                        coordinates_used.append(coord_p2)
                        coordinates_used.append(coord_p3)
                        return (coord, coord_p1, coord_p2, coord_p3), coordinates_used
        else:
            return rand_ship_4_creation(coordinates_used, random.choice('HV'))

    elif direction == 'V':
        row: str = 'DEFG'
        col: str = '0123456789'
        coord: list = random.choice(row) + random.choice(col)

        if coord not in coordinates_used:
            coord_p1: str = chr(ord(coord[0]) + 1) + coord[1]
            coord_m1: str = chr(ord(coord[0]) - 1) + coord[1]

            if coord_m1 in coordinates_used and coord_p1 in coordinates_used:
                return rand_ship_4_creation(coordinates_used, random.choice('HV'))
            elif coord_p1 in coordinates_used:
                coord_m2: str = chr(ord(coord[0]) - 2) + coord[1]

                if coord_m2 in coordinates_used:
                    return rand_ship_4_creation(coordinates_used, 'H')
                else:
                    coord_m3: str = chr(ord(coord[0]) - 3) + coord[1]
                    if coord_m3 in coordinates_used:
                        return rand_ship_4_creation(coordinates_used, 'V')
                    else:
                        coordinates_used.append(coord)
                        coordinates_used.append(coord_m1)
                        coordinates_used.append(coord_m2)
                        coordinates_used.append(coord_m3)
                        return (coord, coord_m1, coord_m2, coord_m3), coordinates_used
            else:
                coord_p2: str = chr(ord(coord[0]) + 2) + coord[1]
                if coord_p2 in coordinates_used:
                    return rand_ship_4_creation(coordinates_used, 'H')
                else:
                    coord_p3: str = chr(ord(coord[0]) + 3) + coord[1]
                    if coord_p3 in coordinates_used:
                        return rand_ship_4_creation(coordinates_used, 'V')
                    else:
                        coordinates_used.append(coord)
                        coordinates_used.append(coord_p1)
                        coordinates_used.append(coord_p2)
                        coordinates_used.append(coord_p3)
                        return (coord, coord_p1, coord_p2, coord_p3), coordinates_used
        else:
            return rand_ship_4_creation(coordinates_used, random.choice('HV'))


def player_ship_1_creation(coordinates_used: list) -> tuple[str, list]:

    coord: str = input('Enter 1-block ship (coordinates A0 to J9): ').upper()

    if coord not in coordinates_used:
        coordinates_used.append(coord)
        return coord, coordinates_used
    else:
        print('Entered ship is on an already used coordinate \nPlease enter a valid coordinate.')
        print('Already used coordinates: ', coordinates_used)
        return player_ship_1_creation(coordinates_used)


def player_ship_2_creation(coordinates_used: list) -> tuple[tuple, list]:

    coord: str = input('Enter 2-block ship (coordinates A0 to J9 separated by comma): ').upper()

    if not BoardVerification.check_if_ships_collides(coord, coordinates_used):
        if BoardVerification.check_if_ship_is_diagonal(coord):
            ship = coord.split(',')
            for i in ship:
                coordinates_used.append(i)

            return ship, coordinates_used
        else:
            print('Ships cannot be diagonal, please enter a valid coordinates.')
            print('Entered coordinates: ', coord)
            return player_ship_2_creation(coordinates_used)
    else:
        print('Entered ship is on an already used coordinate \nPlease enter a valid coordinates.')
        print('Already used coordinates: ', coordinates_used)
        return player_ship_2_creation(coordinates_used)


def player_ship_3_creation(coordinates_used: list) -> tuple[tuple, list]:

    coord: str = input('Enter 3-block ship (coordinates A0 to J9 separated by comma): ').upper()

    if not BoardVerification.check_if_ships_collides(coord, coordinates_used):
        if BoardVerification.check_if_ship_is_diagonal(coord):
            ship = coord.split(',')
            for i in ship:
                coordinates_used.append(i)

            return ship, coordinates_used
        else:
            print('Ships cannot be diagonal, please enter a valid coordinates.')
            print('Entered coordinates: ', coord)
            return player_ship_3_creation(coordinates_used)
    else:
        print('Entered ship is on an already used coordinate \nPlease enter a valid coordinates.')
        print('Already used coordinates: ', coordinates_used)
        return player_ship_3_creation(coordinates_used)


def player_ship_4_creation(coordinates_used: list) -> tuple[tuple, list]:

    coord: str = input('Enter 4-block ship (coordinates A0 to J9 separated by comma): ').upper()

    if not BoardVerification.check_if_ships_collides(coord, coordinates_used):
        if BoardVerification.check_if_ship_is_diagonal(coord):
            ship = coord.split(',')
            for i in ship:
                coordinates_used.append(i)

            return ship, coordinates_used
        else:
            print('Ships cannot be diagonal, please enter a valid coordinates.')
            print('Entered coordinates: ', coord)
            return player_ship_4_creation(coordinates_used)
    else:
        print('Entered ship is on an already used coordinate \nPlease enter valid coordinates.')
        print('Already used coordinates: ', coordinates_used)
        return player_ship_4_creation(coordinates_used)
